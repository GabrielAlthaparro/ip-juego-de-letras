from collections import namedtuple

TAMANNO_LETRA = 20
FPS_inicial = 3
TIEMPO_MAX = 61

ANCHO = 800
ALTO = 600

COLOR_FONDO = (0, 0, 0)
COLOR_TEXTO = (200, 200, 200)
COLOR_TIEMPO_FINAL = (200, 20, 10)


# nuestras variables

# Colores de las letras, diferentes depende columna. ordenadas.
COLOR_LETRAS_UNO, COLOR_LETRAS_DOS, COLOR_LETRAS_TRES = (20, 200, 20), (103, 137, 131), (255, 120, 0)
PRIMER_LINEA_VERTICAL = ANCHO//3
SEGUNDA_LINEA_VERTICAL = ANCHO//3*2
archivo = open("lemario.txt", "r")
cont_palabras_chicas = 0
# enumerate crea un enumerate object con formato [numero_linea,palabra], donde numero linea se asigna a count, y palabra a line
for count, line in enumerate(archivo):
    if len(line) < 3:
        cont_palabras_chicas += 1
archivo.close()
LEN_ARCHIVO = count - cont_palabras_chicas + 1  # cantidad de lineas del archivo
