from configuracion import *
from principal import *

import random
import math


def cargarListas(lista: list, listaIzq: list, listaMedio: list, listaDer: list, posicionesIzq: list, posicionesMedio: list, posicionesDer: list):
    # palabra random del archivo
    palabra = lista[random.randint(0, LEN_ARCHIVO-1)]
    for letra in palabra:
        num_lista = random.randint(1, 3)  # lista aleatoria
        if num_lista == 1:  # guardar letra en lista izquierda
            borde_desde = 1
            borde_hasta = PRIMER_LINEA_VERTICAL-15
            guardarLetraEnListas(listaIzq, posicionesIzq, letra, borde_desde, borde_hasta)
        elif num_lista == 2:  # guardar letra en lista del medio
            borde_desde = PRIMER_LINEA_VERTICAL+15
            borde_hasta = SEGUNDA_LINEA_VERTICAL-15
            guardarLetraEnListas(listaMedio, posicionesMedio, letra, borde_desde, borde_hasta)
        else:  # guardar letra en lista derecha
            borde_desde = SEGUNDA_LINEA_VERTICAL+15
            borde_hasta = ANCHO-15
            guardarLetraEnListas(listaDer, posicionesDer, letra, borde_desde, borde_hasta)
        # elige una palabra de la lista y la carga en las 3 listas
        # y les inventa una posicion para que aparezca en la columna correspondiente


def bajar(lista: list, posiciones: list):
    if len(lista) > 0:
        movimiento_y = 5  # cuanto bajan las letras
        for pos_x_y in posiciones:
            pos_x_y[1] += movimiento_y  # baja la posicion en y de la letra
            if pos_x_y[1] > ALTO-90:  # si la letra llego al piso
                lista.pop(0)
                posiciones.pop(0)
    # hace bajar las letras y elimina las que tocan el piso


def actualizar(lista: list, listaIzq: list, listaMedio: list, listaDer: list, posicionesIzq: list, posicionesMedio: list, posicionesDer: list):
    bajar(listaIzq, posicionesIzq)
    bajar(listaMedio, posicionesMedio)
    bajar(listaDer, posicionesDer)
    if random.randint(1, 10) == 1:
        cargarListas(lista, listaIzq, listaMedio, listaDer, posicionesIzq, posicionesMedio, posicionesDer)
    # llama a otras funciones para bajar bajar las letras, eliminar las que tocan el piso y
    # cargar nuevas letras a la pantalla (esto puede no hacerse todo el tiempo para que no se llene de letras la pantalla)


def estaCerca(elem: int, lista: list):
    # elem es la posicion en x de la letra
    len_lista = len(lista)
    if len_lista > 0:
        i = len_lista-1
        distancia_en_x = 15  # que tan separadas deben estar las letras en el eje x
        distancia_en_y = 10
        # mientas hayan posiciones, y las posiciones en el eje y sean cercanas
        while i >= 0 and lista[i][1] <= distancia_en_y:
            x = lista[i][0]
            # si la distancia en eje x entre las letras es menor a la distancia
            if elem-x >= -distancia_en_x and elem-x <= distancia_en_x:
                return True
            i -= 1
    return False
    # es opcional, se usa para evitar solapamientos entre las letras


def Puntos(candidata: str):
    # vocal = 1
    # easy cons = 2
    # hard cons = 5

    vocales = "aeiou"
    cons_dificiles = "jkqwxyz"
    total_puntos = 0
    for letra in candidata:
        if letra in vocales:
            total_puntos += 1
        elif letra in cons_dificiles:
            total_puntos += 5
        else:
            total_puntos += 2
    return total_puntos


def procesar(lista: list, candidata: str, listaIzq: list, listaMedio: list, listaDer: list, posicionesIzq: list, posicionesMedio: list, posicionesDer: list):
    if esValida(lista, candidata, listaIzq, listaMedio, listaDer, posicionesIzq, posicionesMedio, posicionesDer):
        return Puntos(candidata)
    else:
        return 0
    # chequea que candidata sea correcta en cuyo caso devuelve el puntaje y 0 si no es correcta


def esValida(lista: list, candidata: str, listaIzq: list, listaMedio: list, listaDer: list, posicionesIzq: list, posicionesMedio: list, posicionesDer: list):
    if candidata in lista:
        col = 0  # empieza buscando desde la primer columna
        i = 0
        listas = [listaIzq, listaMedio, listaDer]
        pos_usadas = [[], [], []]
        pos_letra: int
        while col < 3 and i < len(candidata):
            letra = candidata[i]
            pos_letra = posLetraEnListas(listas[col], letra, pos_usadas[col])
            if pos_letra != -1:  # si es una letra valida
                pos_usadas[col].append(pos_letra)
                i += 1  # siguiente letra
            else:
                col += 1  # buscar la misma letra en la siguiente columna
        if col != 3:  # si forme la palabra sin irme de la tercera columna, candidata es valida
            borrarLetrasDeListas(listaIzq, posicionesIzq, pos_usadas[0])
            borrarLetrasDeListas(listaMedio, posicionesMedio, pos_usadas[1])
            borrarLetrasDeListas(listaDer, posicionesDer, pos_usadas[2])
            return True
    return False  # si no es una palabra del diccionario
    # devuelve True si candidata cumple con los requisitos


# Funciones creadas por nosotros

def guardarLetraEnListas(lista: list, posiciones: list, letra: str, borde_izquierdo: int, borde_derecho: int):
    # posicion de inicio de y, arriba. La diagonal izquierda arriba es el punto (0,0)
    pos_y = 0
    pos_x = random.randint(borde_izquierdo, borde_derecho)
    while estaCerca(pos_x, posiciones):
        pos_x = random.randint(borde_izquierdo, borde_derecho)
    lista.append(letra)
    posiciones.append([pos_x, pos_y])


def posLetraEnListas(lista: list, letra: str, pos_usadas: list):
    if letra in lista:
        pos = 0
        while pos < len(lista):
            if lista[pos] == letra and pos not in pos_usadas:
                return pos
            pos += 1
    return -1


def borrarLetrasDeListas(lista: list, posiciones: list, pos_usadas: list):
    pos_usadas.sort(reverse=True)
    for pos in pos_usadas:
        lista.pop(pos)
        posiciones.pop(pos)
