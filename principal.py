#! /usr/bin/env python
import os
import random
import sys
import math

import pygame
from pygame.locals import *

from configuracion import *
from funcionesVACIAS import *
from extras import *

# Funcion principal


def main():
    # Centrar la ventana y despues inicializar pygame
    os.environ["SDL_VIDEO_CENTERED"] = "1"
    pygame.init()
    # pygame.mixer.init()
    pygame.mixer.music.load('sonidos/fondo.mp3')
    pygame.mixer.music.play(2)
    pygame.mixer.music.set_volume(0.1)
    # Preparar la ventana
    pygame.display.set_caption("Armar palabras...")
    screen = pygame.display.set_mode((ANCHO, ALTO))

    # fondo
    fondo = pygame.image.load("fondo.jpg")
    screen.blit(fondo, (0, 0))

    # tiempo total del juego
    gameClock = pygame.time.Clock()
    totaltime = 0
    segundos = TIEMPO_MAX
    fps = FPS_inicial

    puntosAux = 0
    finalEfect = True
    puntos = 0
    candidata = ""
    listaIzq = []
    listaMedio = []
    listaDer = []
    posicionesIzq = []
    posicionesMedio = []
    posicionesDer = []
    lista = []

    archivo = open("lemario.txt", "r")
    for linea in archivo.readlines():
        if len(linea.strip()) >= 3:  # Solo permite palabras mayores o iguales a 3 caracteres
            lista.append(linea[0:-1].strip())
    archivo.close()

    cargarListas(lista, listaIzq, listaMedio, listaDer, posicionesIzq, posicionesMedio, posicionesDer)
    dibujar(screen, candidata, listaIzq, listaMedio, listaDer, posicionesIzq, posicionesMedio, posicionesDer, puntos, segundos)

    while segundos > fps/1000:
        # 1 frame cada 1/fps segundos
        gameClock.tick(fps)
        totaltime += gameClock.get_time()

        if True:
            fps = 3

        # Buscar la tecla apretada del modulo de eventos de pygame
        for e in pygame.event.get():

            # QUIT es apretar la X en la ventana
            if e.type == QUIT:
                pygame.quit()
                return()

            # Ver si fue apretada alguna tecla
            if e.type == KEYDOWN:
                letra = dameLetraApretada(e.key)
                candidata += letra
                if e.key == K_BACKSPACE:
                    candidata = candidata[0:len(candidata)-1]
                if e.key == K_RETURN:
                    puntos += procesar(lista, candidata, listaIzq, listaMedio, listaDer, posicionesIzq, posicionesMedio, posicionesDer)
                    candidata = ""
                    if puntos == puntosAux:
                        soundEfect = pygame.mixer.Sound('sonidos/pifio.wav')
                        soundEfect.play()
                    else:
                        soundEfect = pygame.mixer.Sound('sonidos/adivino.wav')
                        soundEfect.play()
                        puntosAux = puntos

        segundos = TIEMPO_MAX - pygame.time.get_ticks()/1000

        # Limpiar pantalla anterior
        screen.fill(COLOR_FONDO)
        screen.blit(fondo, (0, 0))  # Extra, lo del fondo
        # Dibujar de nuevo todo
        dibujar(screen, candidata, listaIzq, listaMedio, listaDer, posicionesIzq, posicionesMedio, posicionesDer, puntos, segundos)

        pygame.display.flip()

        actualizar(lista, listaIzq, listaMedio, listaDer, posicionesIzq, posicionesMedio, posicionesDer)

        if segundos < 2 and finalEfect:
            pygame.mixer.music.stop()
            game_over = pygame.mixer.Sound('sonidos/game_over.wav')
            game_over.play()
            finalEfect = False

    # FINAL DEL JUEGO
    final = True
    while final:
        screen.fill(COLOR_FONDO)
        fuente = pygame.font.Font(None, 100)
        mensaje = fuente.render("FIN DEL JUEGO", 1, (255, 255, 255), (255, 0, 0))
        defaultFont = pygame.font.Font(pygame.font.get_default_font(), TAMANNO_LETRA)
        puntos = defaultFont.render("Puntos: " + str(puntos), 1, COLOR_TEXTO)
        saludo = defaultFont.render("Gracias por jugar =)", 1, COLOR_TEXTO)
        indicacion = defaultFont.render("Presione la x para cerrar el juego", 1, COLOR_TEXTO)

        screen.blit(mensaje, (160, 100))
        screen.blit(puntos, (360, 270))
        screen.blit(saludo, (315, 350))
        screen.blit(indicacion, (250, 400))

        for e in pygame.event.get():
            if e.type == QUIT:
                pygame.quit()
                return

        final = False
        pygame.display.flip()

    while 1:
        # Esperar el QUIT del usuario
        for e in pygame.event.get():
            if e.type == QUIT:
                pygame.quit()
                return


# Programa Principal ejecuta Main
if __name__ == "__main__":
    main()
